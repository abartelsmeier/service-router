const express = require('express')
const http = require('http');

const app = express()

app.listen(80, function () {
  console.log('Example app listening on port 3000!')
})

app.all('/:service/:path', function(req, res) {

  var options = {
    host:   req.params.service,
    port:   80,
    path:   '/'+req.params.path,
    method: req.method,
    headers: req.headers
  };

  console.log(options);
  var creq = http.request(options, function(cres) {

    // set encoding
    cres.setEncoding('utf8');

    // wait for data
    cres.on('data', function(chunk){
      res.write(chunk);
    });

    cres.on('close', function(){
      // closed, let's end client request as well 
      res.writeHead(cres.statusCode);
      res.end();
    });

    cres.on('end', function(){
      // finished, let's finish client request as well 
      //res.writeHead(cres.statusCode);
      res.end();
    });

  }).on('error', function(e) {
    // we got an error, return 500 error to client and log error
    console.log(e.message);
    res.writeHead(500);
    res.end();
  });

  creq.end();

});

